package com.quiz.Controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.quiz.Model.Questions;
import com.quiz.Model.User;
import com.quiz.Service.UserService;
import com.quiz.Validator.UserValidator;

@Controller
public class UserController {
	@Autowired
	UserService service;
	@Autowired
	UserValidator userValidator;
	
	@RequestMapping(value = "Login")
	public ModelAndView LoginUser(@ModelAttribute("user") User user) throws IOException {
		return new ModelAndView("LoginForm");
	}
	@RequestMapping(value = "/userLogin" , method=RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute("user") @Valid User user,BindingResult result) throws IOException {
		userValidator.validate(user,result);
		if(result.hasErrors()){
				return new ModelAndView("LoginForm");
			}
			else{
			List<Questions> listQuestions = service.getQuestions();
			return new ModelAndView("AttemptQuiz","listQuestions", listQuestions);
			}
	}
	@RequestMapping(value = "/userResult")
	public ModelAndView ResultUser(HttpServletRequest request, HttpSession session) throws IOException {
		int qid=Integer.parseInt(request.getParameter("quesNum"));
		int right=0;
		for(int i=qid;i<=3;i++)
		{
			String option=request.getParameter("option"+i);
			
			List<Questions> list= service.getOptinById(i);
			String rightAnswer=list.get(0).getOptionCorrect();
			if(option.equals(rightAnswer)){
				right++;
				System.out.println(right);
			}
			session.setAttribute("totalQuestions",3);
			session.setAttribute("right",right);
			session.setAttribute("wrong",(3)-right);
			System.out.println(option);
			System.out.println(rightAnswer);
		}
		return new ModelAndView("ResultPage");
	}
	@RequestMapping(value = "Pdf")
    public ModelAndView PdfGenerator(HttpServletRequest req, HttpSession session, Model model)throws IOException  {
          List<Questions> listQuestions = service.getQuestions();
          return new ModelAndView("UserSummary", "listQuestions", listQuestions);
    }
}