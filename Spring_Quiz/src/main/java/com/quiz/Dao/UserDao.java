package com.quiz.Dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.quiz.Model.Questions;
import com.quiz.Model.User;


public interface UserDao {

	public List<Questions> getQuestions();

	public List<Questions> getOptinById(int i);

}
