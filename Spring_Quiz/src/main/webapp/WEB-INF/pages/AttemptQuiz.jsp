<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Quiz Page</title>
</head>
<body>
<h1>Start Quiz</h1>
<form:form action="userResult" method="post" modelAttribute="user"> 
<table>
<core:forEach var="questionPaper" items="${listQuestions}" >   
        
    <input type="hidden" value="${questionPaper.id}" name="quesNum"/>
     <tr><td><pre><core:out value="${questionPaper.question}"></core:out></pre></td></tr>
     <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio"  onclick="abc()" id="option1" name="option${questionPaper.id}" value="${questionPaper.option1}">
        <core:out  value="${questionPaper.option1}"></core:out><br/></td></tr>
     
     <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="abc()" id="option2" name="option${questionPaper.id}" value="${questionPaper.option2}">
        <core:out value="${questionPaper.option2}"></core:out><br/></td></tr>
      
     <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="abc()"  id="option3" name="option${questionPaper.id}" value="${questionPaper.option3}">
        <core:out value="${questionPaper.option3}"></core:out><br/></td></tr>
       
     <tr><td>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick="abc()" id="option4" name="option${questionPaper.id}" value="${questionPaper.option4}">
        <core:out value="${questionPaper.option4}"></core:out><br/></td></tr>
 
</core:forEach>
<tr><td colspan="2" align="center"><input type="submit" value="Finish"></td></tr>
      
</table>
 </form:form>
</body>
</html>