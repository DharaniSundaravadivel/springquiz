package com.quiz.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.quiz.Model.User;

public class UserValidator implements Validator 
{

    public boolean supports(Class clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    public void validate(Object target, Errors errors) 
    {
       // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.firstName", "Username is required.");
        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.lastName", "Password is required.");
       // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.firstName");
        // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.password");
    	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "error.firstname");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.password");

    }


}
