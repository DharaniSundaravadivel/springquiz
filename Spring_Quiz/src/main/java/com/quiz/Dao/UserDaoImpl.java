package com.quiz.Dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quiz.Model.Questions;
import com.quiz.Model.User;
@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;
	@SuppressWarnings("unchecked")
	public List<Questions> getQuestions() {

		return sessionFactory.getCurrentSession().createQuery("from Questions")
				.list();
	}
	@Override
	public List<Questions> getOptinById(int i) {
		return (List<Questions>)sessionFactory.getCurrentSession().createQuery("from Questions where id=:qid").setParameter("qid", i).list();
	}
	
}
